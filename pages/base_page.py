from abc import ABC

from selenium.webdriver import Remote


class BasePage(ABC):
    """
    Abstract class for page objects
    """
    page_title = ""

    def __init__(self, driver: Remote):  # driver type is specified for IDE code completion
        self.driver = driver

        if driver.title != self.page_title:
            raise ValueError(f"Current page {driver.title} is not object page {self.page_title}")
