from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.navbar_page import NavbarPage


class CartPage(NavbarPage):
    page_title = "STORE"

    _product_table_title_column = 2

    # Locators
    _product_titles_locator = (By.CSS_SELECTOR,
                              f".table-responsive .success > *:nth-child({_product_table_title_column})")
    _delete_button_locator = (By.CSS_SELECTOR, ".success a")

    # Navigation methods
    def get_product_name_list(self):
        """
        Returns a list of product names from the product table
        If there are none, returns an empty list
        :return: list[str]
        """
        self._wait_for_product_table()

        element_list = self.driver.find_elements(*self._product_titles_locator)

        product_titles = []
        for title_element in element_list:
            product_titles.append(title_element.text)

        return product_titles

    def delete_all_products(self):
        """
        Deletes all products from the cart
        """
        self._wait_for_product_table()

        product_count = len(self.driver.find_elements(*self._product_titles_locator))

        for x in range(product_count):
            self._wait_for_product_table()

            delete_button = self.driver.find_element(*self._delete_button_locator)
            delete_button.click()

            # Waits for page to reload
            WebDriverWait(self.driver, 5).until(expected_conditions.staleness_of(delete_button))

    def _wait_for_product_table(self):
        """
        Waits for the product table to load
        Throws a TimeoutException when table doesn't appear
        """
        try:
            WebDriverWait(self.driver, 3, 2).until(
                expected_conditions.presence_of_element_located(self._product_titles_locator))
        except TimeoutException:
            raise TimeoutException("Product table timeout")
