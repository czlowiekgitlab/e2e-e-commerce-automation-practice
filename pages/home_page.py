from selenium.webdriver.common.by import By

from pages.navbar_page import NavbarPage


class HomePage(NavbarPage):
    page_title = "STORE"

    # Internal element finding methods
    def _get_item_element(self, item_name):
        """
        Returns the page element of specified item
        If item is not found, throws an exception
        :param item_name: Name of the item in the store
        :return: Element
        """
        return self.driver.find_element(By.XPATH, f"//div[@id='tbodyid']//a[.='{item_name}']")

    def _get_category_element(self, category):
        """
        Returns the page element of specified category
        If category is not found, throws an exception
        :param category: Name of the item category
        :return: Element
        """
        return self.driver.find_element(By.CSS_SELECTOR, f"#itemc[onclick=\"byCat('{category}')\"]")

    # Page navigation methods
    def go_to_item_page(self, item_category, item_name):
        """
        Returns new item page object
        :param item_category: Category of the item
        :param item_name: Name of the item to be added to the cart
        :return: ItemPage
        """
        self._get_category_element(item_category).click()

        self._get_item_element(item_name).click()

        from pages.item_page import ItemPage
        return ItemPage(self.driver)
