from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.navbar_page import NavbarPage


class ItemPage(NavbarPage):
    page_title = "STORE"

    # Locators
    _add_to_cart_button_locator = (By.XPATH, "//a[contains(@onclick, 'addToCart')]")

    # Page navigation
    def add_item_to_cart(self):
        """
        Returns an alert popup message after adding the item to the cart
        :return: str
        """
        self.driver.find_element(*self._add_to_cart_button_locator).click()

        WebDriverWait(self.driver, 10).until(expected_conditions.alert_is_present())

        alert = self.driver.switch_to.alert

        result = alert.text

        alert.accept()

        return result
