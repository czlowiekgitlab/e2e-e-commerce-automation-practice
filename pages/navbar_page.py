from selenium.common import NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.base_page import BasePage


class NavbarPage(BasePage):
    """
    Abstract class for pages with a common navbar
    """
    page_title = "STORE"

    # Locators
    _home_tab_locator = (By.CSS_SELECTOR, ".nav-link[href='index.html']")

    _sign_up_popup_button_locator = (By.CSS_SELECTOR, ".btn[onclick='register()'")
    _sign_up_tab_locator = (By.ID, "signin2")
    _sign_up_username_locator = (By.ID, "sign-username")
    _sign_up_password_locator = (By.ID, "sign-password")

    _log_in_username_locator = (By.ID, "loginusername")
    _log_in_password_locator = (By.ID, "loginpassword")
    _log_in_popup_button_locator = (By.CSS_SELECTOR, ".btn[onclick='logIn()'")
    _log_in_tab_locator = (By.ID, "login2")

    _log_out_tab_locator = (By.ID, "logout2")

    _name_of_user_locator = (By.ID, "nameofuser")
    _cart_tab_locator = (By.ID, "cartur")

    # Internal methods
    def _is_alert_present(self):
        try:
            alert = self.driver.switch_to.alert
            alert.accept()
        except NoAlertPresentException:
            return False

        return True

    # Page navigation methods
    def go_to_home_page(self):
        """
        Returns home page object
        :return: HomePage
        """
        self.driver.find_element(*self._home_tab_locator).click()

        from pages.home_page import HomePage
        return HomePage(self.driver)

    def go_to_cart_page(self):
        """
        Returns new cart page object
        :return: CartPage
        """
        self.driver.find_element(*self._cart_tab_locator).click()

        from pages.cart_page import CartPage
        return CartPage(self.driver)

    def sign_up(self, username, password):
        """
        Signs up to the store
        :return: Username if sign up was successful, None if not
        """
        self.driver.find_element(*self._sign_up_tab_locator).click()

        self.driver.find_element(*self._sign_up_username_locator).send_keys(username)
        self.driver.find_element(*self._sign_up_password_locator).send_keys(password)

        self.driver.find_element(*self._sign_up_popup_button_locator).click()

        if self._is_alert_present():
            return None

        WebDriverWait(self.driver, 5).until(
             expected_conditions.visibility_of_element_located(self._name_of_user_locator))

        return self.driver.find_element(*self._name_of_user_locator).text.split()[-1]

    def log_in(self, username, password):
        """
        Logs in to the site
        :return: Returns username if logged in, None if log in failed
        """
        self.driver.find_element(*self._log_in_tab_locator).click()

        self.driver.find_element(*self._log_in_username_locator).send_keys(username)
        self.driver.find_element(*self._log_in_password_locator).send_keys(password)

        self.driver.find_element(*self._log_in_popup_button_locator).click()

        if self._is_alert_present():
            return None

        WebDriverWait(self.driver, 5).until(
            expected_conditions.visibility_of_element_located(self._name_of_user_locator))

        return self.driver.find_element(*self._name_of_user_locator).text.split()[-1]

    def log_out(self):
        """
        Logs out from the current account
        :return: Returns a HomePage object
        """
        self.driver.find_element(*self._log_out_tab_locator).click()

        from pages.home_page import HomePage
        return HomePage(self.driver)
