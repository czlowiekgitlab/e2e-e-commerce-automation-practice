"""
Pytest file containing fixtures to configure what happens after and before tests
"""

import json
import pytest
import selenium.webdriver


@pytest.fixture
def load_config(scope='session'):
    # Loads the test config which specifies the browser used for tests
    # and implicit wait time

    with open('test_config.json') as config_file:
        config = json.load(config_file)

    assert config['browser'] in ['Edge', 'Chrome']  # Checks if browser is supported
    assert isinstance(config['implicit_wait'], int)
    assert config['implicit_wait'] > 0

    return config


@pytest.fixture
def setup_browser(load_config):
    # Sets up the browser and implicit wait time specified in the test config file

    match load_config['browser']:
        case 'Edge':
            driver = selenium.webdriver.Edge()
        case 'Chrome':
            driver = selenium.webdriver.Chrome()
        case _:
            raise Exception(f'Browser "{load_config["browser"]}" is not supported')

    driver.implicitly_wait(load_config['implicit_wait'])

    yield driver  # Gives control to the test method

    # This performs after the test method is done
    driver.quit()
