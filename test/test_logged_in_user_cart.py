"""
Tests storing items in a cart for a logged-in user
"""

import pages.home_page

# Login credentials
USERNAME = "test"
PASSWORD = "test"

# Items to buy
ITEM_LIST = [("monitor", "ASUS Full HD"),
             ("notebook", "MacBook Pro")]


def test_cart_item_storing(setup_browser):
    driver = setup_browser

    driver.get('https://www.demoblaze.com/')

    current_page = pages.home_page.HomePage(driver)

    current_page.log_in(USERNAME, PASSWORD)

    for item in ITEM_LIST:
        category, name = item

        current_page = current_page.go_to_item_page(category, name)
        current_page.add_item_to_cart()
        current_page = current_page.go_to_home_page()

    current_page = current_page.go_to_cart_page()

    products_in_cart = current_page.get_product_name_list()

    for item in ITEM_LIST:
        category, name = item

        assert name in products_in_cart

    current_page.delete_all_products()

    current_page.log_out()
