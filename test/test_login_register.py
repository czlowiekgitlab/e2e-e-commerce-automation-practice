"""
Tests for logging in and signing up
"""
from pages.home_page import HomePage

# Login credentials
USERNAME = "test"
PASSWORD = "test"


def test_unfilled_signup_fail(setup_browser):
    driver = setup_browser

    driver.get("https://www.demoblaze.com/")

    current_page = HomePage(driver)

    assert current_page.sign_up("", "") is None


def test_login_success(setup_browser):
    driver = setup_browser

    driver.get("https://www.demoblaze.com/")

    current_page = HomePage(driver)

    assert current_page.log_in(USERNAME, PASSWORD) == USERNAME

    current_page.log_out()
